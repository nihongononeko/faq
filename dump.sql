CREATE TABLE `user` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `login` varchar(50) NOT NULL,
 `password` varchar(255) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `themes` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `theme` varchar(50) NOT NULL,
 PRIMARY KEY (`id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
 CREATE TABLE `faq` 
(`id` int(11) NOT NULL AUTO_INCREMENT,
 `theme_id` int(11) NOT NULL,
 `question` varchar(500) NOT NULL,
 `answer` varchar(500) NULL DEFAULT NULL,
 `display` tinyint(1) DEFAULT '0',
 `author` varchar(50) NOT NULL,
 `email` varchar(50) NOT NULL,
 `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
 INSERT INTO `user` (`id`, `login`, `password`) 
 VALUES 
 (1, 'admin', 'admin');